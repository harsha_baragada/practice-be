package com.backend.service;

import com.backend.entities.UserEntity;
import com.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;


    public int saveUser(UserEntity user) {
        int response = 0;
        UserEntity saveUser = userRepository.save(user);
        if (saveUser != null) {
            response = 1;
        }
        return response;
    }
}
