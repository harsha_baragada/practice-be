package com.backend.controller;

import com.backend.entities.UserEntity;
import com.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(value = "/saveUser")
    public int saveUser(@RequestBody UserEntity user) {
        return userService.saveUser(user);
    }
}
